---
title: Contact
layout: page
date: 2018-12-09 04:40:58 +0000
page-id: contact

---
For questions, comments, concerns, press, media enquiries, partnering or any other relevant matter, please see the below contact info:

| | |
|---------------------------------------|----------------------------------------|
| Email | [ia.dnem@yreva](mailto:#) |
|---------------------------------------|----------------------------------------|
| Phone | [7280-995-307-1+](tel:#) |