---
title: AI-Driven Dermatology Could Leave Dark-Skinned Patients Behind
layout: post
date: 2018-08-16 14:00:00 +0000
format: Interview
for: The Atlantic
external-url: https://www.theatlantic.com/health/archive/2018/08/machine-learning-dermatology-skin-color/567619/
org-logo-filename: the-atlantic

---
