---
title: Machine Learning and Health Care Disparities in Dermatology
layout: post
date: 2018-08-01 09:54:25 +0000
format: Research Paper
for: American Medical Association
external-url: https://jamanetwork.com/journals/jamadermatology/article-abstract/2688587
org-logo-filename: ama

---
