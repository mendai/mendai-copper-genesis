---
title: Baltimore software developer calls for more inclusive data in machine learning
  that helps treat skin diseases
layout: post
date: 2018-09-04 16:24:00 +0000
format: Interview
for: Technical.ly
external-url: https://technical.ly/baltimore/2018/09/04/baltimore-software-developer-calls-for-more-inclusive-data-in-machine-learning-that-helps-treat-skin-diseases/
org-logo-filename: technically

---
