var gulp = require('gulp');
const babel = require('gulp-babel');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var run = require('gulp-run');
var runSequence = require('run-sequence');
var gutil = require('gulp-util');
var del = require('del');
var browserSync = require('browser-sync').create();

// move css to jekyll assets dir
// move images to jekyll assets dir
// concatenate all js files into one scripts.js file
// save scripts file to jekyll assets dir

var paths = {}
paths.assetsDir = '_assets/';
paths.jekyllAssetsDir = 'assets/';
paths.siteDir = '_site';

paths.cssFiles = paths.assetsDir + 'css/**/*';
paths.jekyllCssDir = paths.jekyllAssetsDir + 'css';

paths.imageFiles = paths.assetsDir + 'img/**.*';
paths.jekyllImagesDir = paths.jekyllAssetsDir + 'img';

paths.jsFiles = paths.assetsDir + 'js/**/*.js';
paths.jekyllJsDir = paths.jekyllAssetsDir + 'js';

// build assets
gulp.task('build:styles', function() {
    return gulp.src(paths.cssFiles)
    	.pipe(gulp.dest(paths.jekyllCssDir))
    	.pipe(browserSync.stream())
    	.on('error', gutil.log);
});

gulp.task('build:images', function() {
    return gulp.src(paths.imageFiles)
    	.pipe(gulp.dest(paths.jekyllImagesDir))
    	.pipe(browserSync.stream())
    	.on('error', gutil.log);
});

gulp.task('build:scripts', function() {
    return gulp.src(paths.jsFiles)
        .pipe(concat('scripts.js'))
        .pipe(babel({
			presets: ['@babel/env']
		}))
        .pipe(uglify())
        .pipe(gulp.dest(paths.jekyllJsDir))
        .on('error', gutil.log);
});

gulp.task('build:assets', [
	'build:styles',
	'build:images',
	'build:scripts'
]);

// clean assets
gulp.task('clean:assets', function() {
	return del([paths.jekyllAssetsDir]);
});

// clean the jekyll site
gulp.task('clean:jekyll', function() {
	return del([paths.siteDir]);
});

gulp.task('clean', [
	'clean:jekyll',
	'clean:assets'
]);

// build the jekyll site anew
gulp.task('build:jekyll', function() {
<<<<<<< HEAD
	var shellCommand = 'JEKYLL_ENV=production bundle exec jekyll build --config _config.yml';
=======
	var shellCommand = 'bundle exec jekyll build --config _config.yml';
>>>>>>> 667c94f9cd76bbe57febcd6f1215e731cd19b83a
	return gulp.src('')
		.pipe(run(shellCommand))
		.on('error', gutil.log);
});

gulp.task('extra:remove_minima_theme_stuff', function() {
	return del([
		paths.siteDir + '/assets/main.css',
<<<<<<< HEAD
		paths.siteDir + '/assets/minima-social-icons.svg',
		paths.siteDir + '/assets/main.css.gz',
		paths.siteDir + '/assets/minima-social-icons.svg.gz'
=======
		paths.siteDir + '/assets/minima-social-icons.svg'
>>>>>>> 667c94f9cd76bbe57febcd6f1215e731cd19b83a
	]);
});

gulp.task('build', function() {
	return runSequence('clean', [
			'build:styles',
			'build:images',
			'build:scripts'
		],
		'build:jekyll',
		'extra:remove_minima_theme_stuff')
});

// default task to build the site
gulp.task('default', ['build']);

// run jekyll build command with test config
gulp.task('build:jekyll:test', function() {
	var shellCommand = 'bundle exec jekyll build --config _config.yml,_config.test.yml';
	return gulp.src('')
		.pipe(run(shellCommand))
		.on('error', gutil.log)
});

// run jekyll build command with local development config
gulp.task('build:jekyll:local', function() {
	var shellCommand = 'JEKYLL_ENV=local bundle exec jekyll build --drafts --config _config.yml,_config.test.yml,_config.dev.yml';
	return gulp.src('')
		.pipe(run(shellCommand))
		.on('error', gutil.log)
});

// special task for building and then reloading BrowserSync
gulp.task('build:jekyll:watch', ['build:jekyll:local'], function() {
	return browserSync.reload();
});

gulp.task('build:scripts:watch', ['build:scripts'], function() {
	return browserSync.reload();
});

// Static Server + watching files.
// note: passing anything besides hard-coded literal paths with glob doesn't
// seem to work with gulp.watch().
gulp.task('serve', ['build:jekyll:local'], function() {

	browserSync.init({
		server: paths.siteDir,
		ghostMode: false, // toggle to mirror clicks, relaods etc. (performance)
		logFileChanges: true,
		logLevel: 'debug',
		open: true // toggle to automatically open a page when starting
	});

	// watch site settings
	gulp.watch(['_config.yml', '_config.dev.yml'], ['build:jekyll:watch']);

	// watch .scss files; changes are piped to browserSync
	gulp.watch('_sass/**/*.scss', ['build:jekyll:watch']);

	// watch .js files
	gulp.watch('_assets/js/**/*.js', ['build:scripts:watch']);

	// watch image files; changes are piped to browserSync
	gulp.watch('_assets/img/**/*', ['build:jekyll:watch']);

	// watch posts
	gulp.watch('_posts/**/*.+(md|markdown|MD)', ['build:jekyll:watch']);

	// Watch drafts if --drafts flag was passed.
	// ** not sure how to test for this use-case right now ** //
	gulp.watch('_drafts/*.+(md|markdown|MD)', ['build:jekyll:watch']);
    if (module.exports.drafts) {
        gulp.watch('_drafts/*.+(md|markdown|MD)', ['build:jekyll:watch']);
    }

    // Watch html and markdown files.
    gulp.watch(['**/*.+(html|md|markdown|MD)', '!_site/**/*.*'], ['build:jekyll:watch']);

    // Watch RSS feed XML file.
    gulp.watch('feed.xml', ['build:jekyll:watch']);

    // Watch data files.
    gulp.watch('_data/**.*+(yml|yaml|csv|json)', ['build:jekyll:watch']);

});